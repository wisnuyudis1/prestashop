<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container7jasnjs\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container7jasnjs/appProdProjectContainer.php') {
    touch(__DIR__.'/Container7jasnjs.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\Container7jasnjs\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \Container7jasnjs\appProdProjectContainer([
    'container.build_hash' => '7jasnjs',
    'container.build_id' => 'b79437d9',
    'container.build_time' => 1665623725,
], __DIR__.\DIRECTORY_SEPARATOR.'Container7jasnjs');
