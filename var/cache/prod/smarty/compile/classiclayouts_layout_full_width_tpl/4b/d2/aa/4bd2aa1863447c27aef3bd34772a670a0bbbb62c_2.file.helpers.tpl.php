<?php
/* Smarty version 3.1.43, created on 2022-10-13 08:40:29
  from 'C:\xampp74\htdocs\prestashop\themes\classic\templates\_partials\helpers.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.43',
  'unifunc' => 'content_63476c8d835551_82371970',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4bd2aa1863447c27aef3bd34772a670a0bbbb62c' => 
    array (
      0 => 'C:\\xampp74\\htdocs\\prestashop\\themes\\classic\\templates\\_partials\\helpers.tpl',
      1 => 1665582857,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_63476c8d835551_82371970 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => 'C:\\xampp74\\htdocs\\prestashop\\var\\cache\\prod\\smarty\\compile\\classiclayouts_layout_full_width_tpl\\4b\\d2\\aa\\4bd2aa1863447c27aef3bd34772a670a0bbbb62c_2.file.helpers.tpl.php',
    'uid' => '4bd2aa1863447c27aef3bd34772a670a0bbbb62c',
    'call_name' => 'smarty_template_function_renderLogo_23100687463476c8d828e28_41262678',
  ),
));
?> 

<?php }
/* smarty_template_function_renderLogo_23100687463476c8d828e28_41262678 */
if (!function_exists('smarty_template_function_renderLogo_23100687463476c8d828e28_41262678')) {
function smarty_template_function_renderLogo_23100687463476c8d828e28_41262678(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
    <img
      class="logo img-fluid"
      src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
"
      alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
      width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
"
      height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
">
  </a>
<?php
}}
/*/ smarty_template_function_renderLogo_23100687463476c8d828e28_41262678 */
}
